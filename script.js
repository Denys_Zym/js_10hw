// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

const tab = document.querySelector(".tabs");
const tabs = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".content");

tab.addEventListener("click", (event) => {
  tabs.forEach((i) => {
    if (event.target === i) {
      event.target.classList.add("active");
    } else {
      i.classList.remove("active");
    }
  });
  contents.forEach((a) => {
    if (event.target.dataset.id !== a.dataset.id) {
      a.classList.remove("content-active");
    } else {
      a.classList.add("content-active");
    }
  });
});

//-----------------------------------------------------------------

// const tabs = document.querySelectorAll(".tabs-title");
// const contents = document.querySelectorAll(".content");

// for (let i = 0; i < tabs.length; i++) {
//   tabs[i].addEventListener("click", (event) => {
//     let tabsCurrent = event.target.parentElement.children;
//     for (let a = 0; a < tabsCurrent.length; a++) {
//       tabsCurrent[a].classList.remove("active");
//     }
//     event.target.classList.add("active");

//     let contentsCurrent =
//       event.target.parentElement.nextElementSibling.children;
//     for (let b = 0; b < contentsCurrent.length; b++) {
//       contentsCurrent[b].classList.remove("content-active");
//     }
//     contents[i].classList.add("content-active");
//   });
// }
